<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    private $post;
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function ola()
    {
        $nome = "Ciro";

        return view('posts.index',['nome' => $nome]);
    }

    public function index()
    {
        return $this->post->all();
    }
}
